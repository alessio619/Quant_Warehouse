---
title: Computational Finance Introdocution in R
author: alessio619
date: '2021-06-13'
slug: []
categories: []
tags: []
---

# Load the monthly Starbucks return data

In this first lab, you will analyze the monthly stock returns of Starbucks (ticker: SBUX).

Let us get started by downloading the monthly return data from http://assets.datacamp.com/course/compfin/sbuxPrices.csv, and by using the `read.csv(`) function. Type ?read.table in the console to consult the help file.

In the `read.csv()` function, you should indicate that the data in the CSV file has a header (header argument) and that strings should not be interpreted as factors (stringsAsFactors argument).

```{r}
# Assign the URL to the CSV file
data_url <- "http://assets.datacamp.com/course/compfin/sbuxPrices.csv"

# Load the data frame using read.csv
sbux_df <- read.csv(file = data_url, header = TRUE, stringsAsFactors = FALSE)
```

Before you analyze return data, it is a good idea to have (at least) a quick look at the data. R has a number of functions that help you do that:  

- The `str()` function compactly displays the structure of an R object. It is arguably one of the most useful R functions.  
- The `head()` and `tail()` functions shows you the first and the last part of an R object, respectively.  
- The `class()` function shows you the class of an R object.  