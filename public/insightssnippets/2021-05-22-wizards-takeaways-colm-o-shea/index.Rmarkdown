---
title: 'Macro Wizards'
author: ''
date: '2021-05-22'
slug: []
categories: []
tags: []
draft: false
---

The original ideas was to write a post on each character interviewed in **"Hedge Fund Wizards"** by Jack Schwager, but then I realized that there are richer takeaways to make from cross-validating them. The first three *Wizards* are Colm O'Shea, Ray Dalio and Larry Benedict. Three different personalities, investing styles and backgrounds, still two similarities pop up: absolute risk management and deep macroeconomic understanding. In summary I'll say that:

- O'Shea is a Macro Strategist and trades more like an (true) economist, his bets are meant to be 1- 3 months long and mostly anticipates business cycle shifts by identifying fundamental breakdowns that eventually lead to business cycle turns. He prefers, as George Soros taught him, to invest first and investigate later.

- Dalio is the "Idealized Hedge Fund manager"; his trades last between 12 - 18 months and looks for fundamentally uncorrelated assets to create a **Pure Alpha** portfolio. He is a formidable leader, encouraging honest and transparent dialogue transversely IN ORDER TO improve the investing system.    

- Larry Benedict is the weird guy that has a super brain, moved to Miami to be more comfortable, has a terrible temper but inspires trust among peers. He, on the contrary, has a well-defined idea of how economics works, but as he makes LOTS of VERY SHORT-TERM bets, he isolates fundamentals from pure technicals. With pure I mean he doesn't even see graphs, he designs trades in his mind by watching numbers. His only rules are based on streaks (the more he loses, the less capital exposure).

At the same time they have something in common, how they are deep-opinionated on how the world works and they achieve not only doing that but also perceiving the effect it will have on the markets. 

### Colm O'Shea

Of the 3 of them, he impressed me the most. First of all he is an economist, as I am, and he also had the impression that all forecast are "sound" among them and that inherently means that they will never be useful to anticipate shifts or understand what is really going on. The second thing was his ability to take simple principles and turn them into powerful pieces of advice or even method. For example:

- He seeks for Positive Skew, as it depicts the frequency of positive returns and probability of large losses.  
- Trades are HYpothesis that need to be tested in the market, before betting you make yourself clear, what is your hypothesis saying and when it means your are right or wrong. This allows you to have clear entry and exit points and stops (both for gains and losses).  
- Defy "data", beautiful data doesn't exist; something is happening underneath and on the other hand opportunities are in anomalies and not normality.  
- Making clear that he trades swings, he still believes that even during irrational exuberance money can be made, but extra care has to be taken when placing the trade; liquidity may dry when the bubble burst so position yourself in something that has a quick and easy exit.
- Storytelling on the bet is just 10%, the 90% is about implementation and flexibility; There is difference between buying and asset and testing a hypothesis under this scheme.  
- In addition he speaks a lot about **human psychology (optimism - bull mind)** and that creates a gap between economic fundamentals breaking and the market actual cycle shift. Furthermore, because of this he affirms that it is difficult to make money shorting a bubble, it is a bumpy road. 


### Ray Dalio

He is a "Big Picture THinker", he created a template to classify the economics of the business cycle that in a few words has two-dimensions: **productivity growth** and **credit expansion / deleveraging**, with a third component that is the ability to **print money**. All these things helps understanding how the environment will develop and central banks (key player in the market) will act. For me it sounds easy but dizzy, because productivity is hard to measure, credit numbers are harsh to find and read and Central Banks act in several ways simultaneously.

However, he gave me a refreshing sense of how economics affect the market and its importance. On the other hand he built two major straggles: **Pure Alpha** and **All Weather**, that makes a lot of sense when matching with his macroeconomic framework. IN order to know what assets to allocate in, you need to understand how they behaved in the past, within each "stage". Finally, all my lessons on Monetary Economics and International Macro are useful. 

It is also worth mentioning that, while O'Shea introduce a sort of *Univariate Framework*, Dalio focus on a *Multivariate World*, where the asset, the market, the portfolio and the economy need to be studied individually AND vis-a-vis altogether. His main statistic is not distribution of probabilities but **Correlation**. Again, for O'Shea a histogram seems more natural as he looks to a single asset (oversimplification, he traded hypothesis not single assets), while for Dalio looking things moving together (or not, actually) is more relevant. A quick disclosure, he talks about "Fundamental Correlation" which is not the kind of Pearson or Spearman you can calculate. I think he is trying to say instead like he watches after *Dynamics* and *First Order, Second Order and Third Order Effects* need to be found and analyzed, at some point he says that it is hard to explain but we **do not have to think linearly**.  

LAst thing on Ray that caught my attentions was a principle: "it is okay to fail but unacceptable not to identify, analyze and learn from mistakes". He also says, I paraphrase, that "is better to be wrong but thinking the right way than being right on the wrong path". It is clear that he truly believes in long-term strategies and that in the long run the truth (talent, hard work) arise and luck (dishonesty, laziness) erode.

HIs last piece of advice is that "people tend to think that the past will repeat". I see that like an opportunity, because knowledge in financial markets have expedition date and it gives room for newcomers. 

### Larry Benedict

The last Wizard has a trading style that really did not caught my attention, but still I realized to interesting things. He failed a lot on his early (and mid?) career but never gave up. I felt this on my skin, the *Wall St. stereotype*  of a successful handsome mid 30's guy added pressure to a career that was difficult enough to study and learn. Time passed, I'm still young, but having little real market experience with only a couple of bucks lost trading CFD's made myself doubt if I could ever make it. Larry instead, was confident and kept trying, following O'Shea's advice: find your own style.   

That style, however, differed substantially from the prior 2 Wizards, he DOES NOT ALLOW the economic-fundamental outlook interfere in his trades (which makes sense since his trades are very short-term), and has a **<Human with bad temper - Non Graphical - Discretionary>** trading technique, which has no framework rather than a Black-Box model within his brain. I will certainly not follow his steps, but it was refreshing to know that there is people out there capable of winning against HFT and Goldman Sachs' army, just to mention a few. 

### Last Remarks

I noticed how attracted I am towards Dalio's and O'Shea *Fundamentally Systematic* approach mixing economics with statistics. They design rules based on their experience and knowledge that will reduce the amount of discretionary decisions but not in the way that *Algo traders* do, I find difficult to see "signals" rather than concepts. At the same time, they **Systematic** part (trading implementations especially) and **Risk Management** are two areas which I would difinetively invest more time in instead of keep digging into "Portfolio Analysis" and pure Equity staff. Managing risk is more than VaR, it is needed to be *fully aware* of it and that is only achievable when a certain level of understanding is reached.

One beatiful phrase I would like to highlight from O'Shea: "Information is there, is in the Financial Times, but nobody cares, then they says it was impossible to see it coming". It gives me hope that critical thinking is a critical piece of the business, and it may overcome the limits of few data and computational power. 
  
To conclude with, hard work and passion may be enough, but intellectual honesty and criticla thinking are the keys to crack your own view on how the world works and in the long run come up with a tailored-made investing system you feel comfortable with and, I hope, make some money. 

