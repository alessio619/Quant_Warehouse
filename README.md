# Ale Abraham BLOG

The features of this blog are:

- [Ale Abraham BLOG](#ale-abraham-blog)
  - [Starting the Project](#starting-the-project)
  - [Home](#home)
    - [Title and Subtitle](#title-and-subtitle)
    - [Social Icons](#social-icons)
  - [About](#about)
  - [CV](#cv)
  - [Investment Insights](#investment-insights)
    - [Creating a new Investment Insight page](#creating-a-new-investment-insight-page)
  - [R Code](#r-code)
    - [Creating a new R Code page](#creating-a-new-r-code-page)
  - [PC Setup](#pc-setup)
    - [Creating a new PC Setup page](#creating-a-new-pc-setup-page)
  - [Literature Review](#literature-review)
    - [Creating a new Literature Review page](#creating-a-new-literature-review-page)
  - [Styling](#styling)
    - [Responsive Navigation Bar](#responsive-navigation-bar)
  - [Git Commit Style](#git-commit-style)
  - [Gotchas](#gotchas)

## Starting the Project
- Unzip the file and open the root of the project in your terminal. 
- Start the hugo server by typing the following command in your terminal: ```hugo server```
- Open this url in your browser: [`http://localhost:1313/`](http://localhost:1313/)
To view your files that are still have `draft:true`, you start the server with an extra flag like so: `hugo sever -D`. You can find more information on how to [`start hugo server`](https://gohugo.io/getting-started/quick-start/#step-5-start-the-hugo-server)
## Home

### Title and Subtitle

To change the title and subtitle of the homepage change the following properties in the [`config.toml`](config.toml) file

```toml
title   = "Ale Abraham"

[params]
  homeSubtitle = "Finance and Data Science"
```

### Social Icons

To change the social icons change the following properties in the [`config.toml`](config.toml) file:

```toml
[params]
  [[params.social]]
    name = "github"
    url  = "https://github.com/alessandroARG/"

  [[params.social]]
    name = "linkedin"
    url  = "https://www.linkedin.com/in/abraham93/"

  [[params.social]]
    name = "email"
    url  = "mailto:nobody@example.com"
```

To add more social icons you can find more options in [**svg.html**](themes/hello-friend-ng/layouts/partials/svg.html)

## About

You can modify the `about` content in [**about.md**](content/about.md)

## CV

The CV file is stored in the [**static**](static) folder and is named `cv.pdf`.
If you rename the file, also rename it in the [**config.toml**](config.toml) file

```toml
[[menu.main]]
    identifier = "cv"
    name       = "CV"
    url        = "/cv.pdf"
```

## Investment Insights

The [`Investment Insights`](content/investment-insights) page is a single page with multiple nested posts like so:

```bash
<root>
├── content
│   ├── investment-insights
│   │   └── whatever-it-takes.md
│   │   └── investment-insight-one.md
│   │   └── investment-insight-two.md
```

It is made up of:

- an archetype, [`investment-insights.md`](archetypes/investment-insights.md)
- a layout of [investment-insights](layouts/investment-insights/single.html) and
- a folder in the [`content`](content) directory called [`investment-insights`](content/investment-insights)

### Creating a new Investment Insight page

To create a new Investment Insight page, run the `hugo new` command in your terminal. For example, to create an investment insight page called `my-new-investment-insight.md`, we will run:

```bash
hugo new investment-insights/my-new-investment-insight.md
```

## R Code

The [`R Code`](content/r-code) page is a single page with multiple nested posts like so:

```bash
<root>
├── content
│   ├── r-code
│   │   └── r-code-post-one.md
│   │   └── r-code-post-two.md
```

It is made up of:

- an archetype, [`r-code.md`](archetypes/r-code.md)
- a layout of [`r-code`](layouts/r-code/single.html) and
- a folder in the [`content`](content) directory called [`r-code`](content/r-code)

### Creating a new R Code page

To create a new R Code page, run the `hugo new` command in your terminal. For example, to create an r-code page called `r-code-post-one.md`, we will run:

```bash
hugo new r-code/r-code-post-one.md
```

## PC Setup

The [`PC Setup`](content/pc-setup) page is a single page with multiple nested posts like so:

```bash
<root>
├── content
│   ├── pc-setup
│   │   └── my-pc-setup.md
```

It is made up of:

- an archetype, [`pc-setup.md`](archetypes/pc-setup.md)
- a layout of [`pc-setup`](layouts/pc-setup/single.html) and
- a folder in the [`content`](content) directory called [`pc-setup`](content/pc-setup)

### Creating a new PC Setup page

To create a new PC Setup page, run the `hugo new` command in your terminal. For example, to create an r-code page called `my-pc-setup.md`, we will run:

```bash
hugo new pc-setup/my-pc-setup.md
```

## Literature Review

The [`Literature Review`](content/literature-review) page is a single page with multiple nested posts like so:

```bash
<root>
├── content
│   ├── literature-review
│   │   └── literature-review-one.md
│   │   └── literature-review-two.md
```

It is made up of:

- an archetype, [`literature-review.md`](archetypes/literature-review.md)
- a layout of [`literature-review`](layouts/literature-review/single.html) and
- a folder in the [`content`](content) directory called [`literature-review`](content/literature-review)

### Creating a new Literature Review page

To create a new Literature Review page, run the `hugo new` command in your terminal. For example, to create a literature-review page called `literature-review-one.md`, we will run:

```bash
hugo new literature-review/literature-review-one.md
```

## Styling

### Responsive Navigation Bar

Making the navigation bar responsive is achieved with 2 steps

- write media queries in a [`style.css`](assets/css/style.css) file
- create a [`baseof.html`](layouts/_default/baseof.html) file and include the `style.css` file in the `<head></head>` like so:
  ```html
  {{ $style := resources.Get "css/style.css" }} 
  <link rel="stylesheet" href="{{ $style.Permalink }}" />
  ```
  This way the style is applied to all the pages.

## Git Commit Style
The commit style used in this project is the [Conventional Commmits](https://www.conventionalcommits.org/en/v1.0.0/) which is a specification for adding human and machine readable meaning to commit messages.

## Gotchas
Newly created markdown files have a draft set to true, therefore remember to set them to false before publishing.

```text
draft: true
```

```text
---
title: "Your Famous Title"
date: file-creation-date
draft: true
toc: false
images:
tags:
  - testing
  - random
---
```
