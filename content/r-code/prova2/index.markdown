---
title: prova2
author: aless_io.619
date: '2021-07-09'
slug: prova2
categories: []
tags: []
draft: no
---



## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:


```r

# PPA PRICER    



# LIBRARIES ---------------------------------------------------------------


### GENERAL CODING TOOLS    
library(tidyverse)

### CODEFLOW PIPING SYSTEM    
library(data.table)

### PATHS CONTROLLING    
library(here)

### DATE HANDLING   
library(lubridate)

# to be removed #
library(janitor)



# FUNCTIONS ---------------------------------------------------------------


# + Data Upload ----                                

# LOAD TABULAR DATA FROM .CSV   

tabular.inputs.automatic.DT <- function(file_name)  {
    
    rawdataLocation <- '00.raw_data'
    
    # INPUT SECTION CODES   
    macro_pricer_tag <<- "PRC"
    sec_price_tag <<- "PRI"
    sec_volume_tag <<- "VOL"
    sec_tecnology_tag <<- "TEC"
    sec_risk_tag <<- "RSK"
    
    
    
    ## Read files named xyz01.csv, xyz02.csv, etc.   
    filenames <- list.files(path = here(rawdataLocation), pattern = ".*csv")
    
    ### Load all files   
    for(i in filenames) {
        
        filepath <- paste0(here(rawdataLocation, i))
        
        name <- paste0(i) %>% str_remove(".csv")
        
        assign(name, 
               fread(filepath, skip = 4),
               envir = .GlobalEnv)
    }
    
    str_split_fixed(filenames, "[_\\.]", 5)
    
}










# + Time Inputs ----                                   

# PPA SIM LENGTH CALCULATION (A)  

sim.ppa.duration.a <- function(base_year = 2023, duration = 3)  {

            pd_start_year <- ymd(base_year, truncated = 2L)
            pd_duration <- duration
            pd_duration <<- duration

        pd_end_year <<- year(pd_start_year + dyears(pd_duration))

}



# PPA SIM LENGTH CALCULATION (B)  

sim.ppa.duration.b <- function(base_year = 2023, duration = 10, time_step = 1)  {

    vec_duration <<- seq(from = base_year,
                        by = time_step,
                        to = (base_year + duration) - 1)

    return(vec_duration)

}



# + Stable Inputs ----                           

sim.risk.adversion <- function(risk_apetite = "low")  {
    
        vec_risk <- tibble(minimum = 0.55, low = 0.65, moderate = 0.80, high = 0.90, extreme = 0.95)
    
    rp_adversion <<- colnames(select(vec_risk, risk_apetite))
    rp_adversion_percentile <<- as.numeric(select(vec_risk, risk_apetite))


}






# 0. CONTROL PANEL ------------------------------------------------------------    





## 0.1 Scenario Inputs -------------------------------------------------------    

sim.ppa.duration.a(base_year = 2023, duration = 10)

sc_producibility_ppa <- 0.75
sc_ppa_price_scenario <- "central" # central - high - low - ?market? 

sc_producibility_spot <- 0.95
sc_spot_price_scenario <- "low" # central - high - low - ?market? 




## 0.2 Sensitivity Inputs -----------------------------------------------------   

### RISK PROFILING OFF-TAKER   
sim.risk.adversion(risk_apetite = "high")

### COMMERCIAL SUPPORT   
comm_support_years <- 3
comm_price_risk_included <- 0.00

### POWER PLANT   
pp_max_capacity <- 39




## 0.3 Stable Inputs ----------------------------------------------------------   

rr_param_rsk_degradationRate <- 0.004 
rr_param_rsk_production_high <- "p25" 
rr_param_rsk_production_low <-  "p95" 
rr_param_rsk_percBenefit <- 0.5 

fp_param_efficiency <- 0.5 # vector - time !!!
fp_param_emission <- 0.3346 # vector - time !!!
fp_param_gas_transport <- 2 # vector - time !!!








# +++ Input Upload +++ --------------------------------------------------------

tabular.inputs.automatic.DT()







# 1. FIXED PRICES ------------------------------------------------------------

## 1.1 Futures Prices ----------------------------------------------------------


############################################################
# if_else se mercato è liquido, fai i tuoi calcoli
############################################################

### FROM 'FIXED PRICES' TAKE EACH CONTRACT AND CALCULATE MONTHLY & YEARLY MEAN PRICES

# GAS 
DT.fut_gas_monthly <- 
    PRC_PRI_FixedPrice_futures_prices[future_section == "TTF_GAS_CHAIN"][
                                      , .(fut_month_gas_px = mean(close_px), year = year_month_delivery), by = year_month_delivery][
                                        , .(year, fut_month_gas_px)]




DT.fut_gas_yearly <- 
    PRC_PRI_FixedPrice_futures_prices[future_section == "TTF_GAS_CHAIN"][
                                      , .(fut_yearly_gas_px = mean(close_px),
                                      year = year_delivery), by = year_delivery][
                                        , .(year, fut_yearly_gas_px)]


# CO2 
DT.fut_co2_monthly <- 
    PRC_PRI_FixedPrice_futures_prices[future_section == "EUA_CO2_CHAIN"][
                                      , .(fut_month_co2_px = mean(close_px),
                                      year = year_month_delivery), by = year_month_delivery][
                                        , .(year, fut_month_co2_px)]




DT.fut_co2_yearly <- 
    PRC_PRI_FixedPrice_futures_prices[future_section == "EUA_CO2_CHAIN"][
                                      , .(fut_yearly_co2_px = mean(close_px),
                                      year = year_delivery), by = year_delivery][
                                        , .(year, fut_yearly_co2_px)]




## 1.2 Scenarios Prices ---------------------------------------------------------   

### FROM ' YEARLY SCENARIOS' CREEATE THE TABLE TO CALCULATE THE CLEAN SPARK SPREAD

#### FILE M - PRC_PRI_HourlyScenarios_energy_prices_scenarios TO BE UPDATED BY MP ###

DT.sce_gas <- 
    PRC_PRI_YearlyScenarios_energy_prices_scenarios[item_id == "gas_px",
                                                    .(year, gas_px = central)]


DT.sce_co2 <- 
    PRC_PRI_YearlyScenarios_energy_prices_scenarios[item_id == "carbon_px",
                                                    .(year, carbon_px = central)]


DT.sce_electricity <- 
    PRC_PRI_YearlyScenarios_energy_prices_scenarios[item_id == "ws_elec_bl_px",
                                                    .(year, electricity_px = central)]




DT.sce <- 
    merge(DT.sce_gas, DT.sce_co2, by = "year")

DT.sce <- 
   merge(DT.sce, DT.sce_electricity, by = "year")


DT.sce[, css := electricity_px - (gas_px / fp_param_efficiency) - (carbon_px * fp_param_emission)]

```

```
##      speed           dist    
##  Min.   : 4.0   Min.   :  2  
##  1st Qu.:12.0   1st Qu.: 26  
##  Median :15.0   Median : 36  
##  Mean   :15.4   Mean   : 43  
##  3rd Qu.:19.0   3rd Qu.: 56  
##  Max.   :25.0   Max.   :120
```

## Including Plots

You can also embed plots, for example:

<img src="{{< blogdown/postref >}}index_files/figure-html/pressure-1.png" width="672" />

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
