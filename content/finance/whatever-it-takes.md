---
title: 'Whatever It Takes'
date: 2021-06-06T00:41:48+01:00
draft: false
toc: false
images:
tags:
  - colm
  - market
---

You can google quickly about Colm, however what captivates me the most about him is how he sees the financial market through pure economic lenses. When I say pure I mean it, nowadays economics rely too much on mathematics in order to find unique solutions, but Mr. O'Shea uses the primitive practice of thesis-hypothesis formulating and then it test's it in the market. AS As result, built a method in which the market is your input and your output.
