---
title: 'Writing Files: A refreshed Comparisson'
author: aless_io.619
date: '2021-07-11'
slug: writing-files-a-refreshed-comparisson
categories: []
tags: [r, feather, machine learning, data science]
draft: no
---

Will Frank Harrell or someone else please explain to me a real application in
which this is not fast enough?
   -- Brian D. Ripley (announcing improved versions of read.table and
      write.table)
      R-devel (December 2004)

New or not in `R` truth is that there are several ways of doing things and packages get old, evolve and others are born. Recently working with colleagues that work with `python` and they came up with the `.feather` format extension. As I believe that everyone has internet and know how to Google, therefore I will just share my PC's benchmark among the libraries I've used and you can make your own conclusions about it. 



```r
# The Libraries
library(feather)
library(readr)
library(vroom)
library(data.table)
library(readr)
```



```r
# The Big Data Frame
x <- runif(1e7)
x[sample(1e7, 1e6)] <- NA # 10% NAs
df <- as.data.frame(replicate(30, x))


# Base R
base = system.time(write.csv(df, '_test_baseR.csv'))

# Readr
readr = system.time(write_csv(df, '_test_readr.csv'))

# Vroom
vroom = system.time(vroom_write(df, '_test_vroom.csv', delim = ','))

# Data.table
data.table = system.time(fwrite(df, '_test_datatable.csv'))

# Feather
feather = system.time(write_feather(df, '_test_apache.feather')
```



The only caveat of course is that not all available software can read `.feather`, therefore I will continue using `fread()` from my beloved `data.table` package, but is good to have an alternative for heavy duty files.
