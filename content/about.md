+++
title = "About the Project"
date = "2021-07-09"
aliases = ["about-me","about-us","contact"]
cover = "img/diy_about.jpg"
[ author ]
  name = "Alejandro Abraham Spedaletti"
+++

## What is it

It is a personal project in which I hope that `Quant Warehouse` becomes a **reliable** and **unique** `source` of inspiration and ideas. Our vision is to:  

- Learn by Doing, Learn by copying.  
- Do It Your Self > Out-of-the-Box.  
- Find your flavour, I'll show you mine.
- Always provide feedback, this is about networking.

The goal is to share **insights** and **code snippets** (applied in `R`) on several `Finance` subjects like *Portfolio Management*, *Trading* or *Risk Management* with a strong `Statistical` and `Applied Economics` foundation.  


## How to read it

Based on years of 'failed' blogs and websites, this project is different in several ways:  
- It is not a showcase of cool stuff neither a tutorial-based site, even if I may upload that stuff. I realized that re-writing other people material is not exciting and adds no value.  
- Excuse me if **posWhyts are not self-contained**, but I will try to add links to relevant sources when necessary. The focus is in developing an idea or insight, stop.
- Posts are targeted **for my future me**. The whole site is meant to build a `"Wisdom Repository"`. Linearity among posts may occur, but is not mandatory.  
- Even if I'm likely to give a general framework or view, I deeply encourage to *Google* a lot, consume as many alternative sources as you are able to.


## Why

I *felt* that I had plenty of things to said. In my free (all the) time I tend to rationalize **complex systems**, meaning thinking about relationships, feedback loops, etc, but the real fun is when I try to crack them to design **frameworks** to correctly read reality. After university I found out that *absolute truths* that I studied were more about a professors given idea of the world, therefore listening to other bells and coming up with my *version* of the truth was crucial: but all the fun was lost the moment I had no one to share it with. 


## Who I am

About me? I am an **economist** and self-made **data scientist**; major financial literature consumer; but first and foremost a **skeptic and curious dataholic**. 

For many exogenous reasons, I concluded that `DIY` is a rich process for learning and *mastering skills*, especially when it comes to **Research**. Still, you can absorbe a susbtantial part of other's knowledge, then: watch YouTube, listen to podcast, read books, read even more blogs and *doubt about everything*. Doing your own research is the way. 
 
 
## Obvious Disclosure: 

I'm absolutely not a specialist in neither in Finance / Data Science, nor `R` , still you may use this site for ideas or for discussion. From one common folk to another, please feel free to contact me, **I believe that confronting ideas is the best intellectual exercise that can be made**.  

 
***


Happy Investing / Coding / Reading !!  
